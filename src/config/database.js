module.exports = {
  dialect: 'postgres',
  host: 'localhost',
  database: 'nex_database',
  username: 'postgres',
  password: 'nexDigital',
  port: 5432,
  define: {
    timestamps: true,
    underscored: true
  }
}