const { Sequelize } = require('sequelize')
const connection = require('../database')
const User = require('./Users')

const Shop = connection.define('shops', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
    allowNull: false
  },
  user_id: {
    type: Sequelize.UUID,
    allowNull: false,
    foreingKey: true,
    references: { model : 'users', key: 'id' },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  description: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  logo: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  created_at: {
    type: Sequelize.DATE,
  },
  updated_at: {
    type: Sequelize.DATE,
  }
})

Shop.belongsTo(User , { foreingKey: 'user_id' })

Shop.sync({force: false})
  .then(() => console.log('Table shops created!'))
  .catch(err => console.log(err))

module.exports = Shop