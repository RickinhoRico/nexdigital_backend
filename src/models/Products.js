const { Sequelize } = require('sequelize')
const connection = require('../database')
const Shop = require('./Shops')

const Product = connection.define('products', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
    allowNull: false
  },
  shop_id: {
    type: Sequelize.UUID,
    allowNull: false,
    foreingKey: true,
    references: { model : 'shops', key: 'id' },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  description: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  price: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
  size: {
    type: Sequelize.ENUM(['PP', 'P', 'M', 'G', 'GG', 'XG']),
    allowNull: false,
  },
  image: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  created_at: {
    type: Sequelize.DATE,
  },
  updated_at: {
    type: Sequelize.DATE,
  }
})

Product.belongsTo(Shop , { foreingKey: 'shop_id' })

Product.sync({force: false})
  .then(() => console.log('Table products created!'))
  .catch(err => console.log(err))

module.exports = Product