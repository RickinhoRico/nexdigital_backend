const express = require('express')
const cors = require('cors')

const router = require('./routes')
const connection = require('./database')

const app = express();

app.use(cors());
app.use(express.json());
app.use(router);

app.listen(3333, async () => {
  await connection.sync();
  console.log('🚀 Server start on port 3333');
})