const Product = require('../models/Products')

module.exports = {

  async store(request, response){
    const { shop_id } = request.params
    const { name, description, price, size } = request.body;

    const productAlreadyExists = await Product.findOne({ where: { name }})

    if(productAlreadyExists){
      return response.status(406).json({ error: 'Product already exists!'})
    }

    const product = Product.create({ name, description, price, size, shop_id })

    return response.status(201).json({product})
  },

  async index(request, response){
    const { shop_id } = request.params;
    const products = await Product.findAll({where: {shop_id}});

    return products.lenght !== 0
      ? response.status(200).json(products)
      : response.status(204).send()
  },

  async indexOne(request, response){
    const { shop_id, product_id } = request.params;

    const product = await Product.findOne({where: {id:product_id, shop_id}})

    return product 
      ? response.status(200).json(product)
      : response.status(404).json({ error: 'Product not found!'})
  },

  async update(request, response){
    const {  shop_id, product_id } = request.params
    await Product.update(request.body, {where: {id:product_id , shop_id}})
    return response.status(204).send();
  },

  async destroy(request, response){
    const {  shop_id, product_id } = request.params;
    await Product.destroy({where: {id:product_id , shop_id}})
    return response.status(204).send()
  },
}