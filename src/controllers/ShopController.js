const Shop = require('../models/Shops')

module.exports = {

  async store(request, response){
    const { user_id } = request.params
    const { name, description } = request.body;

    const shopAlreadyExists = await Shop.findOne({ where: { name }})

    if(shopAlreadyExists){
      return response.status(406).json({ error: 'Shop already exists!'})
    }

    const shop = Shop.create({ name, description, user_id })

    return response.status(201).json({shop})
  },

  async index(request, response){
    const { user_id } = request.params;
    const shops = await Shop.findAll({where: {user_id}});

    return shops.lenght !== 0
      ? response.status(200).json(shops)
      : response.status(204).send()
  },

  async indexOne(request, response){
    const { user_id, shop_id } = request.params;

    const shop = await Shop.findOne({where: {id:shop_id, user_id}})

    return shop 
      ? response.status(200).json(shop)
      : response.status(404).json({ error: 'Shop not found!'})
  },

  async update(request, response){
    const { user_id, shop_id } = request.params
    await Shop.update(request.body, {where: {id:shop_id , user_id}})
    return response.status(204).send();
  },

  async destroy(request, response){
    const { user_id, shop_id } = request.params;
    await Shop.destroy({where: {id:shop_id , user_id}})
    return response.status(204).send()
  },
}