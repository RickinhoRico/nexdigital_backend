const User = require('../models/Users')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const authConfig = require('../config/secret')

function genereteToken(params = {}){
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400, //1 day
  })
}

module.exports = {

  async store(request, response){
    const { name, email, password } = request.body;

    const userAlreadyExists = await User.findOne({ where: { email }})

    if(userAlreadyExists){
      return response.status(406).json({ error: 'User already exists!'})
    }

    const passwordHash = await bcrypt.hash(password, 8)

    const user = User.create({name, email, password: passwordHash})

    user.password = undefined;
    
    return response.status(201).json({user})
  },

  async index(request, response){
    const users = await User.findAll();

    return users.lenght !== 0
      ? response.status(200).json(users)
      : response.status(204).send()
  },

  async indexOne(request, response){
    const { user_id } = request.params;

    const user = await User.findOne({where: {id:user_id}})

    return user 
      ? response.status(200).json(user)
      : response.status(404).json({ error: 'User not found!'})
  },

  async update(request, response){
    const { user_id } = request.params
    const user = await User.update(request.body, {where: {id:user_id}})
    return response.status(204).json(user);
  },

  async destroy(request, response){
    const { user_id } = request.params;
    const user = await User.destroy({where: {id:user_id}})
    return response.status(204).json(user)
  },

  async auth(request, response){
    const { email, password } = request.body

    const user = await User.findOne({where: {email}})

    if(!user){
      return response.status(400).json({ error: 'Incorrect emil/password combination.'})
    }

    if(!await bcrypt.compare(password, user.password)){
      return response.status(400).json({ error: 'Incorrect emil/password combination.'})
    }

    user.password = undefined;

    return response.status(200).json({
      user, 
      token: genereteToken({ id: user.id })
    });
  }
}