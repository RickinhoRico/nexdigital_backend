const express = require('express')

const UserController = require('../controllers/UserController')
const ShopController = require('../controllers/ShopController')
const authMiddleware = require('../middlewares/auth')
const ProductController = require('../controllers/ProductController')

const router = express.Router()


router.post('/authenticate', UserController.auth)

router.post('/users', UserController.store)
router.get('/users', UserController.index)
router.get('/users/:user_id', UserController.indexOne)
router.patch('/users/:user_id', authMiddleware, UserController.update)
router.delete('/users/:user_id', authMiddleware, UserController.destroy)

router.post('/users/:user_id/shops', authMiddleware, ShopController.store)
router.get('/users/:user_id/shops', authMiddleware, ShopController.index)
router.get('/users/:user_id/shops/:shop_id', authMiddleware, ShopController.indexOne)
router.patch('/users/:user_id/shops/:shop_id', authMiddleware, ShopController.update)
router.delete('/users/:user_id/shops/:shop_id', authMiddleware, ShopController.destroy)

router.post('/shops/:shop_id/products', authMiddleware, ProductController.store)
router.get('/shops/:shop_id/products', authMiddleware, ProductController.index)
router.get('/shops/:shop_id/products/:product_id', authMiddleware, ProductController.indexOne)
router.patch('/shops/:shop_id/products/:product_id', authMiddleware, ProductController.update)
router.delete('/shops/:shop_id/products/:product_id', authMiddleware, ProductController.destroy)

module.exports = router